from a3cat.config import load_config
from a3cat.resources import setup_resources
from a3cat.page_table import generate_table_page
from a3cat.page_about import generate_about_page
from a3cat.page_plots import generate_plots_page
from a3cat.page_downloads import generate_downloads_page
from a3cat.page_upcoming_assemblies import generate_upcoming_assemblies_page
from a3cat.commons import setup_logging, create_dir


CONFIG_FILE_PATH = 'config/config.yaml'


if __name__ == '__main__':

    setup_logging()

    config = load_config(CONFIG_FILE_PATH)
    create_dir(config['website']['root'])

    setup_resources(config)

    generate_table_page(config)
    generate_about_page(config)
    generate_plots_page(config)
    generate_downloads_page(config)
    generate_upcoming_assemblies_page(config)
