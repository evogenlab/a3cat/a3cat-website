
var collapsableButtons = document.getElementsByClassName("collapsable-button");
var i;

for (i = 0; i < collapsableButtons.length; i++) {

  collapsableButtons[i].addEventListener("click", function() {

    this.classList.toggle("active");
    var content = this.parentElement.parentElement.parentElement.nextElementSibling;

    if (content.style.maxHeight){
      content.style.maxHeight = null;
      this.getElementsByClassName('far')[0].classList.remove('fa-minus-square')
      this.getElementsByClassName('far')[0].classList.add('fa-plus-square')
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
      this.getElementsByClassName('far')[0].classList.add('fa-minus-square')
      this.getElementsByClassName('far')[0].classList.remove('fa-plus-square')
    }

  });

}

collapsableButtons[0].click();
