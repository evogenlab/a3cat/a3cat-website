// Creare's 'Implied Consent' EU Cookie Law Banner v:2.4.1
// Conceived by Robert Kent, James Bavington & Tom Foyster

var dropCookie = true;                      // false disables the Cookie, allowing you to style the banner
var cookieDuration = 14;                    // Number of days before the cookie expires, and the banner reappears
var cookieName = 'complianceCookie';        // Name of our cookie
var cookieValue = 'on';                     // Value of cookie


function createDiv(){
    var bodytag = document.getElementsByTagName('body')[0];
    var footer = document.getElementsByTagName('footer')[0];
    var div = document.createElement('div');
    div.setAttribute('class','cookie-banner');
    div.setAttribute('id','cookie-banner');
    div.innerHTML = '<span>Our website uses cookies to generate anonymous traffic statistics. By continuing, we assume your permission to deploy cookies <button class="close-cookie-banner" onclick="removeMe();"><span>OK</span></a></span>';
    // Be advised the Close Banner 'X' link requires jQuery
    // bodytag.appendChild(div); // Adds the Cookie Law Banner just before the closing </body> tag
    bodytag.insertBefore(div, footer); // Adds the Cookie Law Banner just after the opening <body> tag
    $('.cookie-banner').css({
        'display': 'flex',
        'align-items': 'center',
        'justify-content': 'center',
        'width': '100%',
        'height': '100px',
        'text-align': 'center',
        'font-size': '20px',
        'position': 'fixed',
        'bottom': '0px',
        'background-color': '#036328',
        'color': 'white',
        'z-index': '5'
    })
    $('.close-cookie-banner').css({
        'margin-left': '25px',
        'font-size': '20px',
        'color': '#01792f',
        'background-color': 'white',
        'border': 'solid',
        'cursor': 'pointer',
        'width': '70px',
        'height': '40px',
        'border-color': 'black',
        'border-width': '1px'
    })
    document.getElementsByTagName('body')[0].className += ' cookiebanner'; //Adds a class tothe <body> tag when the banner is visible
}


function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000)); 
        var expires = "; expires="+date.toGMTString(); 
    }
    else var expires = "";
    if(window.dropCookie) { 
        document.cookie = name+"="+value+expires+"; path=/"; 
    }
}


function checkCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}


function eraseCookie(name) {
    createCookie(name,"",-1);
}


window.onload = function(){
    if(checkCookie(window.cookieName) != window.cookieValue){
        createDiv(); 
    }
}


function removeMe(){
    // Create the cookie only if the user click on "Close"
    createCookie(window.cookieName,window.cookieValue, window.cookieDuration); // Create the cookie
    // then close the window/
    var element = document.getElementById('cookie-banner');
    element.parentNode.removeChild(element);
}
