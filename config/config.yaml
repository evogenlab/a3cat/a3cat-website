# About
current_a3cat_version: 2025-01-01
busco_version: 5.4.0

# Data files
table_file_path: resources/summary.tsv

# Config files / properties
properties_file_path: config/table.json
busco_visible_datasets:
  - Arthropoda

# Resources
resources:
  css: resources/css
  images: resources/img
  scripts: resources/scripts
  banner_images: resources/img/banner
  a3cat: data/a3cat

# Website structure
website:
  root: public
  data:
    root: data
    busco_results: busco
    a3cat_versions: a3cat
  pages:
    table: table.html
    about: about.html
    plots: plots.html
    downloads: downloads.html
    upcoming_assemblies: upcoming_assemblies.html

# Templates
templates_dir: templates
templates:
  head: head.jinja
  header: header.jinja
  footer: footer.jinja
  data_tables: data_tables.jinja
  table: table.html.jinja
  about: about.html.jinja
  plots: plots.html.jinja
  downloads: downloads.html.jinja
  downloads_table: downloads_table.jinja
  upcoming_assemblies: upcoming_assemblies.html.jinja

# Plots
plots:
  stats_per_order: 
    img: img/plots/stats_per_order.png
    label: Distribution of assembly quality metrics per order with > 5 assemblies
  busco_vs_assembly_stats: 
    img: img/plots/busco_vs_assembly_stats.png
    label: Distribution BUSCO Complete score for the Arthropoda lineage vs. N50 and assembly size
  tree_summary: 
    img: img/plots/tree_summary.png
    label: Number of species and assemblies for all orders across the Arthropoda phylogeny

# Downloads
downloads:
  genbank_header: Genbank Accession
  busco_v4_versions:
    - 2022-01-11
    - 2021-06-11

# Upcoming assemblies
upcoming_assemblies:
  # https://goat.genomehubs.org/api/v2/search?query=tax_tree(arthropoda) AND tax_rank(species) AND sequencing_status!=insdc_open AND (contig_n50<1000000 OR scaffold_n50<10000000)&result=taxon&taxonomy=ncbi&size=150000&offset=0&fields
  goat_api_query: 'https://goat.genomehubs.org/api/v2/search?query=tax_tree%28arthropoda%29%20AND%20tax_rank%28species%29%20AND%20sequencing_status%21%3Dinsdc_open&result=taxon&taxonomy=ncbi&size=150000&offset=0&fields='
  #goat_api_query: 'https://goat.genomehubs.org/api/v2/search?query=tax_tree%28arthropoda%29%20AND%20tax_rank%28species%29%20AND%20sequencing_status%21%3Dinsdc_open%20AND%20%28contig_n50%3C1000000%20OR%20scaffold_n50%3C10000000%29&result=taxon&taxonomy=ncbi&size=150000&offset=0&fields='
  goat_species_url: 'https://goat.genomehubs.org/record?recordId=${taxid}&result=taxon&taxonomy=ncbi'
  projects:
    - DTOL
    - METAINVERT
    - GAGA
    - AG100PEST
    - CANBP
    - ERGA-PIL
    - ERGA-BGE
    - ERGA-CH
    - CCGP
    - LOEWE-TBG
    - CBP
    - ILEBP
    - EBPN
    - ENDEMIXIT
    - PGP
    - TSI

# URL stubs
urls:
  # NOTE: use
  #   target='_blank' rel='noopener noreferrer'
  #   with HTML links so they open in new tab
  # NCBI: "In June 2023, Assembly record pages will be redirected to the new Datasets Genome page."
  #genbank_accession: "<a href='https://www.ncbi.nlm.nih.gov/assembly/{field}'>{field}</a>"
  genbank_accession: "<a href='https://www.ncbi.nlm.nih.gov/data-hub/genome/{x}/' target='_blank' rel='noopener noreferrer'>{x}</a>"
  refseq_accession: "<a href='https://www.ncbi.nlm.nih.gov/data-hub/genome/{x}/' target='_blank' rel='noopener noreferrer'>{x}</a>"
  bioproject: "<a href='https://www.ncbi.nlm.nih.gov/bioproject/{x}/' target='_blank' rel='noopener noreferrer'>{x}</a>"
  taxid: "<a href='https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id={x}&lvl=3&lin=f&keep=1&srchmode=1&unlock' target='_blank' rel='noopener noreferrer'>{x}</a>"
