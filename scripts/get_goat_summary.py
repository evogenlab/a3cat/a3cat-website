#!/usr/bin/env python3
# vim: syntax=python tabstop=2 expandtab
# coding: utf-8
#------------------------------------------------------------------------------
# Get assembly projects via GOAT API.
# Output assembly count for each project.
#------------------------------------------------------------------------------
# author   : Harald Detering
# email    : harald.detering@gmail.com
# modified : 2023-03-03
#------------------------------------------------------------------------------

import requests
import json

projects = [
  '1KFG',
  'AFRICABP',
  'AG100PEST',
  'AGI'
  'ARG',
  'ASG',
  'B10K',
  'CANBP',
  'CBP',
  'CCGP',
  'CFGP',
  'DTOL',
  'EBP',
  'EBPN',
  'ENDEMIXIT',
  'ERGA',
  'EUROFISH',
  'GAGA',
  'GAP',
  'GBR',
  'GIGA',
  'I5K',
  'ILEBP',
  'LOEWE-TBG',
  'METAINVERT',
  'OGG',
  'OMG',
  'OTHER',
  'PGP',
  'PRGP',
  'SQUALOMIX',
  'TSI',
  'VGP',
  'ZOONOMIA'
]

goat_api_query = f'https://goat.genomehubs.org/api/v2/search?query=tax_tree%28arthropoda%29%20AND%20tax_rank%28species%29%20AND%20sequencing_status%21%3Dinsdc_open&result=taxon&taxonomy=ncbi&size=150000&offset=0&fields={"%2C".join(["sequencing_status_" + p.lower() for p in projects])}'

if __name__ == '__main__':

  prj_cnt = { p: 0 for p in projects }
  
  response = requests.get(goat_api_query)
  #print(response.status_code)

  data = response.text
  d = json.loads(data)

  assert 'results' in d
  records = d['results']
  #print(results)

  for rec in records:
    assert 'result' in rec
    assert 'fields' in rec['result']
    #print(json.dumps(rec['result']['fields']))

    for f in rec['result']['fields']:
      if f.startswith('sequencing_status_'):
        prj = f.split('_', 2)[2]
        prj_cnt[prj.upper()] += 1

  print(json.dumps(prj_cnt, sort_keys=True, indent=2))
