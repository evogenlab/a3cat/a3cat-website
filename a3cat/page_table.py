import logging
from collections import defaultdict
from .commons import load_json_to_dict
from .page import generate_page


def generate_table_page(config):
    '''
    '''

    table_file_path = config['table_file_path']
    properties_file_path = config['properties_file_path']
    busco_visible_datasets = set(config['busco_visible_datasets'])

    logging.info('Getting data from summary table')
    data = []
    with open(table_file_path) as table_file:
        header = table_file.readline()[:-1].split('\t')
        
        url_stubs = {}
        for i, field_name in enumerate(header):
            key = field_name.replace(' ', '_').lower()
            if key in config['urls']:
                url_stubs[i] = config['urls'][key]

        for i, line in enumerate(table_file):
            if len(line) < 2:
                continue
            fields = line[:-1].split('\t')
            if len(fields) != len(header):
                logging.warning(f'Incorrect number of fields <{len(fields)}> for line {i + 2} (expected {len(header)}), skipped line.')
                continue
            for j, field in enumerate(fields):
                if field == '':
                    fields[j] = 'NA'
                elif j in url_stubs:
                    fields[j] = ','.join([eval(f'f"""{url_stubs[j]}"""') for x in field.split(',')])
            data.append(fields)

    logging.info(f'Loading table properties from {properties_file_path}')
    properties = load_json_to_dict(properties_file_path)

    # Assign data type in column body (controls data formatting) and default
    # column visibility
    col_type = defaultdict(list)
    hidden_columns = []
    for column, info in properties.items():
        if column not in header:
            continue
        if 'type' in info:
            col_type[info['type']].append(header.index(column))
        if 'visible' in info and not info['visible']:
            hidden_columns.append(header.index(column))
    for i, h in enumerate(header):
        if 'BUSCO' in h:
            col_type['float'].append(i)
            dataset = h.split(' ')[0]
            if dataset not in busco_visible_datasets:
                hidden_columns.append(i)

    # Assign column labels
    labels = [properties[h]['label'] if h in properties else
              h.replace(' BUSCO', '') for h in header]

    generate_page('table', config,
                  table_header=labels,
                  table_rows=data,
                  num_col=col_type['num'],
                  float_col=col_type['float'],
                  string_col=col_type['string'],
                  hidden_col=hidden_columns)
