import logging
from collections import defaultdict
from .commons import load_json_to_dict
from .page import generate_page


def generate_upcoming_assemblies_page(config):
    '''
    '''

    goat_api_query = config['upcoming_assemblies']['goat_api_query']
    goat_species_url = config['upcoming_assemblies']['goat_species_url']
    projects_names = config['upcoming_assemblies']['projects']
    projects_fields = [f'sequencing_status_{project.lower()}' for project in
                       config['upcoming_assemblies']['projects']]
    goat_api_query = f'{goat_api_query}{"%2C".join(projects_fields)}'

    generate_page('upcoming_assemblies',
                  config,
                  goat_api_query=goat_api_query,
                  goat_species_url=goat_species_url,
                  projects_names=projects_names,
                  projects_fields=projects_fields)
