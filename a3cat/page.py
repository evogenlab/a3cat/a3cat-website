import jinja2
import logging
from .commons import get_page_path


def generate_page(page, config, **kwargs):
    '''
    '''
    templates_dir = config['templates_dir']
    template = config['templates'][page]
    page_path = get_page_path(page, config)

    logging.info('Loading template with jinja')
    jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(templates_dir))
    template = jinja_env.get_template(template)

    logging.info('Rendering template')
    html_page = template.render(config=config,
                                **kwargs)

    with open(page_path, 'w') as f:
        f.write(html_page)

    logging.info(f'Succesfully generated HTML page {page_path}')
