import logging
import os
from .page import generate_page


def generate_downloads_page(config):
    '''
    '''
    # Generate links to all a3cat versions tables
    a3cat_versions_folder = os.path.join(config['website']['data']['root'], config['website']['data']['a3cat_versions'])
    busco_v4_versions = set(version.strftime('%Y-%m-%d') for version in config['downloads']['busco_v4_versions'])
    try:
        a3cat_versions = [f for f in os.listdir(a3cat_versions_folder) if os.path.splitext(f)[-1] == '.tsv']
        a3cat_versions = sorted(a3cat_versions, reverse=True)
        a3cat_versions_dict = {}
        for version in a3cat_versions:
            version_date = os.path.splitext(version)[0]
            version_file_path = os.path.join(a3cat_versions_folder, version)
            version_name = version_date if version_date not in busco_v4_versions else f'{version_date} (BUSCO v4.1.4)'
            a3cat_versions_dict[version_name] = version_file_path
    except FileNotFoundError:
        a3cat_versions_dict = {}
    current_a3cat_version = {'tsv': os.path.join(a3cat_versions_folder, str(config['current_a3cat_version']) + '.tsv'),
                             'json': os.path.join(a3cat_versions_folder, str(config['current_a3cat_version']) + '.json')}

    # Get info for downloads table
    table_file_path = config['table_file_path']
    logging.info('Getting data from summary table')
    download_url_string = os.path.join(config['website']['data']['root'],
                                       config['website']['data']['busco_results'],
                                       '{genbank_id}',
                                       '{genbank_id}_{dataset}.tar.gz')
    data = []
    with open(table_file_path) as table_file:
        header = table_file.readline()[:-1].split('\t')
        genbank_index = header.index(config['downloads']['genbank_header'])
        busco_datasets = {i: h.split(' ')[0] for i, h in enumerate(header) if 'Complete' in h}
        for i, line in enumerate(table_file):
            if len(line) < 2:
                continue
            fields = line[:-1].split('\t')
            genbank_id = fields[genbank_index]
            if len(fields) != len(header):
                logging.warning(f'Incorrect number of fields <{len(fields)}> for line {i + 2} (expected {len(header)}), skipped line.')
                continue
            assembly_data = ['<a href=\'' + download_url_string.format(genbank_id=genbank_id, dataset=busco_datasets[i].lower()) + '\'><span style=\'color: black\'><i class=\'fas fa-download\'></i></span></a>' if f != '' else 'NA' for
                             i, f in enumerate(fields) if i in busco_datasets]
            data.append([genbank_id, *assembly_data])

    labels = ['GenBank', *[dataset for dataset in busco_datasets.values()]]
    dataset_columns = [x for x in range(1, len(busco_datasets))]

    generate_page('downloads', config,
                  table_rows=data,
                  table_header=labels,
                  dataset_columns=dataset_columns,
                  a3cat_versions=a3cat_versions_dict,
                  current_a3cat_version=current_a3cat_version)
