import logging
import shutil
import os


def setup_resources(config):
    '''
    '''
    website_root = config['website']['root']
    css_dir_path = config['resources']['css']
    scripts_dir_path = config['resources']['scripts']
    images_dir_path = config['resources']['images']
    a3cat_dir_path = config['resources']['a3cat']

    css_dest_path = os.path.join(website_root, os.path.split(css_dir_path)[-1])
    scripts_dest_path = os.path.join(website_root, os.path.split(scripts_dir_path)[-1])
    images_dest_path = os.path.join(website_root, os.path.split(images_dir_path)[-1])
    a3cat_dest_path = os.path.join(config['website']['root'],
                                   config['website']['data']['root'],
                                   config['website']['data']['a3cat_versions'])
    logging.info('Copying resources')
    try:
        shutil.rmtree(css_dest_path)
    except FileNotFoundError:
        logging.warning(f'<{css_dest_path}> did not exist')
    shutil.copytree(css_dir_path, css_dest_path)
    try:
        shutil.rmtree(scripts_dest_path)
    except FileNotFoundError:
        logging.warning(f'<{scripts_dest_path}> did not exist')
    shutil.copytree(scripts_dir_path, scripts_dest_path)
    try:
        shutil.rmtree(images_dest_path)
    except FileNotFoundError:
        logging.warning(f'<{images_dest_path}> did not exist')
    shutil.copytree(images_dir_path, images_dest_path)
    try:
        shutil.rmtree(a3cat_dest_path)
    except FileNotFoundError:
        logging.warning(f'<{a3cat_dest_path}> did not exist')
    shutil.copytree(a3cat_dir_path, a3cat_dest_path)
