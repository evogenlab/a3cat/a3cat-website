import logging
import os
import yaml


def load_config(config_file_path):
    '''
    '''
    logging.info(f'Loading config from {config_file_path}')
    with open(config_file_path, 'r') as config_file:
        config = yaml.safe_load(config_file)

    logging.info('Getting list of images for the banner')
    try:
        config['resources']['banner_img_list'] = [f for f in os.listdir(config['resources']['banner_images'])]
    except FileNotFoundError:
        logging.error(f'Image resources directory {config["resources"]["banner_images"]} does not exist')
        exit(0)
    return config
