from .page import generate_page


def generate_plots_page(config):
    '''
    '''
    stats_per_order = config['plots']['stats_per_order']
    busco_vs_assembly_stats = config['plots']['busco_vs_assembly_stats']
    tree_summary = config['plots']['tree_summary']

    generate_page('plots', config,
                  stats_per_order=stats_per_order,
                  busco_vs_assembly_stats=busco_vs_assembly_stats,
                  tree_summary=tree_summary)
