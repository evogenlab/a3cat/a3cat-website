import json
import logging
import os


def setup_logging():
    '''
    '''
    logging.basicConfig(level=logging.INFO,
                        format='[%(asctime)s]::%(levelname)s  %(message)s',
                        datefmt='%Y.%m.%d - %H:%M:%S')


def get_page_path(page, config):
    '''
    '''
    if page not in config['website']['pages']:
        raise ValueError(f'Invalid page name <{page}>, check websites -> pages in config')
    root = config['website']['root']
    page_path = config['website']['pages'][page]
    return os.path.join(root, page_path)


def create_dir(dir_path):
    '''
    Utility function that checks if a directory exists and creates it if it does not.
    '''
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)


def json_object_hook(obj):
    '''
    Hook for json loads to convert ints and floats properly.
    '''
    converted_values = {}
    for k, v in obj.items():
        if isinstance(v, str):
            try:
                converted_values[k] = int(v)
            except ValueError:
                try:
                    converted_values[k] = float(v)
                except ValueError:
                    converted_values[k] = v
        else:
            converted_values[k] = v
    return converted_values


def load_json_to_dict(input_file_path):
    '''
    Load a json file directly to a dictionary
    '''
    with open(input_file_path) as input_file:
        return json.load(input_file, object_hook=json_object_hook)
