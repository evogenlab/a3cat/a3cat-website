# Pull changes
git pull
# Activate virtual env
source venv/bin/activate
# Re-generate website code
python3 a3cat.py
# Copy archived a3cat summaries to website folder
sudo cp -r ~/a3cat-website/data/a3cat/* /var/www/a3cat.dcsr.unil.ch/data/a3cat
# Update link to latest version
sudo rm /var/www/a3cat.dcsr.unil.ch/data/a3cat/latest
sudo ln -s /var/www/a3cat.dcsr.unil.ch/data/a3cat/2023-10-05.tsv /var/www/a3cat.dcsr.unil.ch/data/a3cat/latest
# Copy new website code to folder and restart nginx
sudo cp -r ~/a3cat-website/public/* /var/www/a3cat.dcsr.unil.ch; sudo systemctl restart nginx
