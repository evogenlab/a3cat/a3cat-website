# A3Cat website

Generate the static website at https://a3cat.unil.ch/

## Installation

Use a virtual environment:

```bash
git clone git@gitlab.com:evogenlab/a3cat-website.git
cd a3cat-website
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

Parameters are defined in `config/config.yaml`.

To generate static pages, simply run:

```bash
source venv/bin/activate
python a3cat.py
```

The static website will be generated at the path specified by the parameter `website/root` in the config file (default: `public`).
